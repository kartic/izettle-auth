package com.athaydes.izettle.data

import java.security.Principal
import java.time.Instant

data class User(val username: String,
                val phone: String)

data class LoggedInUserName(val value: String) : Principal {
    override fun getName() = value
}

data class PersistedUser(val user: User,
                         val userId: String)

data class LoginData(val username: String,
                     val time: Instant)

data class Token(val sub: String,
                 val issuer: String,
                 val scope: String,
                 val expiresAt: Instant)
