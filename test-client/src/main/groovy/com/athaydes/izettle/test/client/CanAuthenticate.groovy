package com.athaydes.izettle.test.client

import groovy.transform.CompileStatic
import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

import javax.annotation.Nullable
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.UriBuilder

import static javax.ws.rs.core.MediaType.TEXT_HTML

/**
 * Trait that allows tests to impersonate an user who can authenticate.
 */
@CompileStatic
trait CanAuthenticate {

    private final URI authenticateEndpoint = URI.create( 'http://localhost:8080/authenticate' )

    private final OkHttpClient httpClient = new OkHttpClient().newBuilder()
            .followRedirects( false )
            .followSslRedirects( false )
            .build()

    Response authenticate( String username, String password, String clientId,
                           @Nullable String scope = null,
                           @Nullable String state = null ) {

        def uriBuilder = UriBuilder.fromUri( authenticateEndpoint )

        def formBuilder = new FormBody.Builder()
                .add( 'clientId', clientId )
                .add( 'username', username )
                .add( 'password', password )

        if ( scope ) formBuilder.add( 'scope', scope )
        if ( state ) formBuilder.add( 'state', state )

        def request = new Request.Builder()
                .url( uriBuilder.build().toURL() )
                .header( HttpHeaders.ACCEPT, TEXT_HTML )
                .post( formBuilder.build() )
                .build()

        httpClient.newCall( request ).execute()
    }

}