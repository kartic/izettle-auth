package com.athaydes.izettle.test.client

import groovy.json.JsonSlurper
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType
import java.time.Duration
import java.time.Instant

/**
 * This trait has the capability to check the health of the server.
 */
trait VerifiesServerHealth {

    private final URI healthChecksEndpoint = URI.create( 'http://localhost:8081/healthcheck' )

    private final OkHttpClient client = new OkHttpClient()

    void waitForServerToBeHealthy( Duration timeout ) {
        Instant giveupInstant = Instant.now() + timeout

        while ( Instant.now() < giveupInstant ) {
            try {
                verifyServerHealthy()
                println "Server Health check succeeded"
                return
            } catch ( RuntimeException e ) {
                println "Waiting for server to pass health check. Failed with: ${e.message}"
                sleep 1000
            }
        }
    }

    void verifyServerHealthy() {
        def request = new Request.Builder()
                .url( healthChecksEndpoint.toURL() )
                .header( HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON )
                .build()

        Response response

        try {
            response = client.newCall( request ).execute()
        } catch ( IOException e ) {
            throw new RuntimeException( "Could not get response from server: ${e.message}" )
        }

        assert response.code() == 200
        def body = response.body()
        assert body.contentType() == okhttp3.MediaType.parse( MediaType.APPLICATION_JSON )

        def json = new JsonSlurper().parse( body.byteStream() )

        def errors = [ ]

        if ( !json[ 'basic-config' ]?.healthy ) {
            errors << 'basic-config not healthy'
        }
        if ( !json[ 'deadlocks' ]?.healthy ) {
            errors << 'deadlocks health check failed'
        }

        if ( errors ) {
            throw new RuntimeException( "Server FAILED Health Check: " + errors.join( ', ' ) )
        }
    }

}
