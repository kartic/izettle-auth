package com.athaydes.izettle.test.client

import groovy.json.JsonSlurper
import groovy.transform.CompileStatic
import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

import javax.annotation.Nullable
import javax.ws.rs.core.UriBuilder

/**
 * This is a test trait that enables tests to use OAuth easily.
 *
 * Currently, it can only run the code flow! But that's ok, it's the safest flow.
 */
@CompileStatic
trait KnowsOAuthFlows {

    private final URI authorizeEndpoint = URI.create( 'http://localhost:8080/authorize' )
    private final URI tokenEndpoint = URI.create( 'http://localhost:8080/token' )

    private final OkHttpClient httpClient = new OkHttpClient().newBuilder()
            .followRedirects( false )
            .followSslRedirects( false )
            .build()

    abstract String getClientId()

    abstract String getClientSecret()

    /**
     * Send an authorization request to the OAuth server.
     * See <a href="https://tools.ietf.org/html/rfc6749#section-4.1.1">OAuth spec 4.1.1</a>
     * @param responseType
     * @param clientId
     * @param redirectUri
     * @param scope
     * @param state
     * @return http response (expected to be a redirect to the login page)
     */
    Response authorize( String responseType = 'code',
                        String clientId = clientId,
                        @Nullable String redirectUri = null,
                        @Nullable String scope = null,
                        @Nullable String state = null ) {

        def uriBuilder = UriBuilder.fromUri( authorizeEndpoint )
                .queryParam( 'response_type', responseType )
                .queryParam( 'client_id', clientId )

        if ( redirectUri ) uriBuilder.queryParam( 'redirect_uri', redirectUri )
        if ( scope ) uriBuilder.queryParam( 'scope', scope )
        if ( state ) uriBuilder.queryParam( 'state', state )

        def request = new Request.Builder()
                .url( uriBuilder.build().toURL() )
                .build()

        httpClient.newCall( request ).execute()
    }

    Response exchangeCodeForToken( String code,
                                   String clientId = clientId,
                                   String clientSecret = clientSecret ) {

        def uriBuilder = UriBuilder.fromUri( tokenEndpoint )

        def request = new Request.Builder()
                .url( uriBuilder.build().toURL() )
                .post( new FormBody.Builder()
                .add( 'grant_type', 'authorization_code' )
                .add( 'code', code )
                .add( 'client_id', clientId )
                .add( 'client_secret', clientSecret ).build() )
                .build()

        httpClient.newCall( request ).execute()
    }

    String extractCodeFrom( Response response ) {
        // get the code from the response
        assert response.code() == 303

        def location = response.header( 'Location' )
        assert location != null, "Location not provided in response: $response"

        def query = URI.create( location ).query
        def codePart = query.split( '&' ).find { it.startsWith( 'code=' ) }
        assert codePart != null, "Code was not provided in query: $query"

        return codePart.substring( 'code='.size() )
    }

    Map decodeToken( String token ) {
        if ( token ) try {
            return new JsonSlurper().parse( Base64.urlDecoder.decode( token ) ) as Map
        } catch ( e ) {
            e.printStackTrace()
        }

        return [ : ]
    }

}