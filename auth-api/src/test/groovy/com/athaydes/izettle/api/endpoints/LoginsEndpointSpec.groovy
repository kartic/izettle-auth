package com.athaydes.izettle.api.endpoints

import com.athaydes.izettle.api.storage.LoginDataStorage
import com.athaydes.izettle.data.LoggedInUserName
import com.athaydes.izettle.data.LoginData
import spock.lang.Specification

import java.time.Duration
import java.time.Instant

class LoginsEndpointSpec extends Specification {

    def "The logins endpoint correctly uses a LoginStorage to look up user's data"() {
        given: 'The logins endpoint uses a Mocked data storage expecting a particular call to happen later'
        Instant t1 = Instant.now()
        Instant t2 = t1 - Duration.ofSeconds( 50 )

        def loginsData = [ new LoginData( username, t1 ), new LoginData( username, t2 ) ]

        def storage = Mock( LoginDataStorage )
        1 * storage.getByUserName( username, limit ) >> loginsData

        def loginsEndpoint = new LoginsEndpoint( storage )

        when: 'The latest logins are requested'
        def result = loginsEndpoint.latestLogins( new LoggedInUserName( username ), limit )

        then: 'The expected results are obtained with a successful response'
        result.status == 200
        result.entity == [ logins: [ t1.toEpochMilli(), t2.toEpochMilli() ] ]

        where:
        username | limit
        'joe'    | 2
        'mary'   | 4
    }

}
