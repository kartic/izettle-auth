package com.athaydes.izettle.api.authentication

import com.athaydes.izettle.api.ServerSettings
import com.athaydes.izettle.api.storage.NonceStorage
import com.athaydes.izettle.api.storage.UserStorage
import com.athaydes.izettle.data.PersistedUser
import com.athaydes.izettle.data.User
import io.dropwizard.views.View
import org.eclipse.jetty.http.HttpStatus
import org.hibernate.validator.constraints.NotEmpty
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.net.URI
import javax.validation.Valid
import javax.validation.constraints.NotNull
import javax.ws.rs.Consumes
import javax.ws.rs.FormParam
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.UriBuilder

/**
 * Authorization server name.
 */
const val iZettle = "izettle"

const val DEFAULT_SCOPE = "login_data" // lets users see their own login data

data class UserCredentials(@NotEmpty val username: String,
                           @NotNull val password: String)

data class LoginRequest(@Valid val userCredentials: UserCredentials,
                        @NotEmpty val clientId: String,
                        @NotEmpty val scope: String,
                        @NotEmpty val state: String)

/**
 * Configuration of an OAuth client.
 */
data class ClientConfig(val clientId: String,
                        val clientSecret: String,
                        val redirectURI: URI)

/**
 * The authentication Service is responsible for authenticating users.
 */
@Path(ServerSettings.Endpoints.AUTHENTICATE)
class AuthenticationService(
        /**
         * Storage of nonces. On a successful authentication, the user gets a nonce
         * that may be exchanged for a token via the token endpoint.
         */
        val nonceStorage: NonceStorage,
        /**
         * A storage for instances of [User].
         */
        val userStorage: UserStorage,
        /**
         * The client configuration for which this service can provide authentication.
         * Only one client is allowed for simplicity.
         */
        val clientConfig: ClientConfig) {

    private val log: Logger = LoggerFactory.getLogger(AuthenticationService::class.java)

    @GET
    @Produces(MediaType.TEXT_HTML)
    fun startAuthentication(): LoginView {
        log.info("Getting login page")
        return showLoginPage(
                clientId = clientConfig.clientId,
                scope = DEFAULT_SCOPE)
    }

    /**
     * HTML Form authentication handler.
     */
    // only enforce that the clientId is provided at this level, as that's a client error...
    // validate user errors by just returning the login page again with an error the user can see.
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    fun authenticate(@FormParam("username") username: String?,
                     @FormParam("password") password: String?,
                     @NotEmpty @FormParam("clientId") clientId: String,
                     @FormParam("scope") scope: String? = null,
                     @FormParam("state") state: String? = null): Any {
        if (username == null || username.isBlank()) {
            return showLoginPage(clientConfig.clientId,
                    errorMessage = "Please enter your username and password")
        }

        if (clientId != clientConfig.clientId) {
            throw WebApplicationException("Client is not authorized", HttpStatus.UNAUTHORIZED_401)
        }

        return authenticate(LoginRequest(
                UserCredentials(username, password ?: ""),
                clientId, scope ?: "", state ?: "")) { user ->
            if (user == null) showLoginPage(clientConfig.clientId, errorMessage = "Bad credentials")
            else sucessfulAuthentication(state, user)
        }
    }

    private fun <R> authenticate(loginRequest: LoginRequest,
                                 responseHandler: (PersistedUser?) -> R): R {
        val user = userStorage.getByCredentials(loginRequest.userCredentials)
        log.info("Authentication result: {}", user)
        return responseHandler(user)
    }

    private fun sucessfulAuthentication(state: String?,
                                        user: PersistedUser): Response {

        val nonce = nonceStorage.createNonce(user.user.username)

        log.info("Giving nonce to the user at URI {}", clientConfig.redirectURI)

        val uriBuilder = UriBuilder.fromUri(clientConfig.redirectURI)
                .queryParam("code", nonce)

        if (!state.isNullOrEmpty()) {
            uriBuilder.queryParam("state", state)
        }

        return Response.seeOther(uriBuilder.build()).build()
    }

    companion object {

        fun showLoginPage(clientId: String,
                          errorMessage: String? = null,
                          inforMessage: String? = null,
                          scope: String? = null,
                          state: String? = null,
                          user: String? = null) =
                LoginView(clientId, errorMessage, inforMessage,
                        scope ?: DEFAULT_SCOPE, state ?: "",
                        user ?: "")
    }

}

data class LoginView(val clientId: String,
                     val errorMessage: String? = null,
                     val infoMessage: String? = null,
                     val scope: String,
                     val state: String,
                     val user: String) :
        View("/views/login.ftl")