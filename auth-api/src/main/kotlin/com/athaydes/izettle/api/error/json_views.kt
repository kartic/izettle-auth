package com.athaydes.izettle.api.error

/**
 * The JSON view for all errors.
 *
 * It intentionally copies the default Dropwizard error format.
 */
data class JsonErrorView(val code: Int,
                         val message: String)