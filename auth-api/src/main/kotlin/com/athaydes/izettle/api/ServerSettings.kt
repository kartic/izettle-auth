package com.athaydes.izettle.api

/**
 * Non-configurable server settings.
 */
object ServerSettings {

    object OAuth {
        const val AUTHORIZE_ENDPOINT = "authorize"
        const val TOKEN_ENDPOINT = "token"
        const val CALLBACK_ENDPONT = "oauth-callback"
    }

    object Endpoints {
        const val AUTHENTICATE = "authenticate"
        const val REGISTRATION = "registration"
        const val LOGINS = "api/v1/logins"
    }
}
