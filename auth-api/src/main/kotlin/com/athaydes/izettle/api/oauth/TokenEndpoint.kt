package com.athaydes.izettle.api.oauth

import com.athaydes.izettle.api.ServerSettings
import com.athaydes.izettle.api.authentication.ClientConfig
import com.athaydes.izettle.api.authentication.DEFAULT_SCOPE
import com.athaydes.izettle.api.authentication.iZettle
import com.athaydes.izettle.api.storage.NonceStorage
import com.athaydes.izettle.api.token.TokenService
import com.athaydes.izettle.data.Token
import org.hibernate.validator.constraints.NotEmpty
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.Instant
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

/**
 * OAuth 2.0 token endpoint.
 *
 * The token endpoint is used by the client to obtain an access token by
 * presenting its authorization grant or refresh token.
 *
 * Note: this endpoint only supports the authorization code flow.
 *
 * @see https://tools.ietf.org/html/rfc6749#section-3.2
 */
@Path(ServerSettings.OAuth.TOKEN_ENDPOINT)
class TokenEndpoint(
        val nonceStorage: NonceStorage,
        val tokenService: TokenService,
        val applicationConfig: ClientConfig) {

    private val log: Logger = LoggerFactory.getLogger(TokenEndpoint::class.java)

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    fun issueToken(@FormParam("grant_type") @NotEmpty grantType: String,
                   @FormParam("code") @NotEmpty code: String,
                   @FormParam("client_id") @NotEmpty clientId: String,
                   @FormParam("client_secret") @NotEmpty clientSecret: String,
                   @FormParam("scope") scope: String?): Response {

        if (notExpectedClient(clientId, clientSecret)) {
            return badRequest("unauthorized_client")
        }

        if (grantType != "authorization_code") {
            return badRequest("unsupported_grant_type")
        }

        val username = nonceStorage.verifyNonce(code) ?:
                return badRequest("invalid_grant")

        val expiresIn = tokenService.tokenTimeToLive

        val token = Token(
                sub = username,
                scope = scope ?: DEFAULT_SCOPE,
                issuer = iZettle,
                expiresAt = Instant.now().plus(expiresIn))

        val signedToken = tokenService.sign(token)

        return Response.ok(mapOf(
                "access_token" to signedToken,
                "token_type" to "Bearer",
                "expires_in" to expiresIn.seconds
        )).build()
    }

    private fun badRequest(reason: String) =
            Response.status(400)
                    .entity(mapOf("error" to reason))
                    .build()

    private fun notExpectedClient(clientId: String, clientSecret: String) =
            applicationConfig.clientId != clientId ||
                    applicationConfig.clientSecret != clientSecret

}