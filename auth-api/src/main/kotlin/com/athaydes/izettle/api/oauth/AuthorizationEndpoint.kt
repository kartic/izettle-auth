package com.athaydes.izettle.api.oauth

import com.athaydes.izettle.api.ServerSettings
import com.athaydes.izettle.api.authentication.AuthenticationService
import com.athaydes.izettle.api.authentication.ClientConfig
import org.hibernate.validator.constraints.NotEmpty
import org.hibernate.validator.constraints.URL
import java.net.URI
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.UriBuilder

/**
 * OAuth 2.0 authorization endpoint.
 *
 * This endpoint is used by registered applications to obtain an authorization code
 * which can be exchanged by a token.
 *
 * See [OAuth spec 4.1](https://tools.ietf.org/html/rfc6749#section-4.1).
 */
@Path(ServerSettings.OAuth.AUTHORIZE_ENDPOINT)
class AuthorizationEndpoint(
        /**
         * The application (OAuth app) configuration.
         * In a real authorization endpoint, many applications could be configured,
         * but to keep things simple, we only allow one application for now.
         */
        val applicationConfig: ClientConfig) {

    @GET
    @Produces(MediaType.TEXT_HTML) // when redirecting the user to login
    fun authorize(@QueryParam("response_type") @NotEmpty responseType: String,
                  @QueryParam("client_id") @NotEmpty clientId: String,
                  @QueryParam("redirect_uri") @URL redirectUri: String?,
                  @QueryParam("scope") scope: String?,
                  @QueryParam("state") state: String?): Any {
        val requestedRedirectUri =
                if (redirectUri == null) applicationConfig.redirectURI
                else URI.create(redirectUri)

        if (requestedRedirectUri != applicationConfig.redirectURI) {
            return unauthorized("The requested redirect URI is not authorized")
        }

        if (clientId != applicationConfig.clientId) {
            return unauthorized("The client is not authorized")
        }

        // we always use our own authenticator to login, but could as well use any OAuth authenticator
        return when (responseType) {
            "code" -> AuthenticationService.showLoginPage(
                    clientId = clientId,
                    errorMessage = null,
                    scope = scope,
                    state = state)
        // TODO implement other flows
            else -> Response.seeOther(UriBuilder.fromUri(redirectUri)
                    .queryParam("error", "unsupported_response_type").build())
                    .build()
        }
    }

    private fun unauthorized(reason: String) =
            Response.status(401)
                    .entity("<div>$reason<div>")
                    .build()

}