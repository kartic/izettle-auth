package com.athaydes.izettle.api.token

import com.athaydes.izettle.data.Token
import java.time.Duration

/**
 * Token Service that can sign and verify tokens.
 */
interface TokenService {

    fun sign(token: Token): String

    fun verify(encodedToken: String?): Token?

    fun isValid(encodedToken: String?): Boolean = verify(encodedToken) != null

    val tokenTimeToLive: Duration

}
