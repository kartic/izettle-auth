package com.athaydes.izettle.api.endpoints

import com.athaydes.izettle.api.ServerSettings
import com.athaydes.izettle.api.storage.LoginDataStorage
import com.athaydes.izettle.data.LoggedInUserName
import io.dropwizard.auth.Auth
import org.hibernate.validator.constraints.Range
import javax.ws.rs.DefaultValue
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

/**
 * The logins endpoint.
 *
 * A logged in user may call this endpoint to obtain information about his or her own logins.
 */
@Path(ServerSettings.Endpoints.LOGINS)
class LoginsEndpoint(val loginsStore: LoginDataStorage) {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun latestLogins(@Auth username: LoggedInUserName,
                     @QueryParam("count") @DefaultValue("5")
                     @Range(min = 1L, max = 1000L) count: Int): Response {
        val logins = mapOf(
                "logins" to loginsStore.getByUserName(username.value, count)
                        .map { it.time.toEpochMilli() }
                        .take(count).toList())

        return Response.ok().entity(logins).build()
    }

}