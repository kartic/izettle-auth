<#-- @ftlvariable name="" type="com.athaydes.izettle.api.endpoints.RegistrationView" -->
<html>
<head>
    <title>Register</title>
    <style>
        .error {
            color: red;
        }

        .lower {
            padding-top: 15px;
        }
    </style>
</head>
<body>
<h1>Renato Athaydes Authorization Server</h1>
<h2>Please register:</h2>

<form action="/registration" method="post">
    <div>
        <label for="username">Username:</label>
    </div>
    <div>
        <input type="text" id="username" name="username"/>
    </div>
    <div>
        <label for="phone">Phone:</label>
    </div>
    <div>
        <input type="text" id="phone" name="phone"/>
    </div>
    <div>
        <label for="password">Password</label>
    </div>
    <input type="password" id="password" name="password"/>
    </div>
<#if errorMessage??>
    <div class="error">${errorMessage?html}</div>
</#if>
    <div class="lower">
        <button type="submit">Create User</button>
    </div>
</form>
</body>
</html>