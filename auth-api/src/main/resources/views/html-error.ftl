<#-- @ftlvariable name="" type="com.athaydes.izettle.api.error.ErrorView" -->
<html>
<head>
    <title>Error</title>
    <style>
    </style>
</head>
<body>
<h2>${status.statusCode} - ${status.reasonPhrase?html}</h2>
<#if errors?has_content>
<div>
    The following errors occurred:
</div>
<ul>
    <#list errors as error>
        <li>${error?html}</li>
    </#list>
</ul>
<#else>
<div>Sorry!</div>
</#if>
</body>
</html>