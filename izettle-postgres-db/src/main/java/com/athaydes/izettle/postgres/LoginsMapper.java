package com.athaydes.izettle.postgres;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginsMapper implements ResultSetMapper<Logins> {

    @Override
    public Logins map( int index, ResultSet r, StatementContext ctx ) throws SQLException {
        Logins logins = new Logins();
        logins.setId( r.getString( "id" ) );
        logins.setUsername( r.getString( "username" ) );
        logins.setTime( r.getTimestamp( "time" ) );
        return logins;
    }
}
