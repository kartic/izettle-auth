package com.athaydes.izettle.postgres;

import com.athaydes.izettle.api.authentication.UserCredentials;
import com.athaydes.izettle.api.storage.StorageException;
import com.athaydes.izettle.api.storage.UserStorage;
import com.athaydes.izettle.data.PersistedUser;
import com.athaydes.izettle.data.User;
import org.jetbrains.annotations.Nullable;
import org.postgresql.util.PSQLException;
import org.skife.jdbi.v2.exceptions.DBIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Arrays;

import static com.athaydes.izettle.postgres.error.PostgresErrorMapper.storageExceptionFrom;

/**
 * Postgres-based implementation of User storage.
 * <p>
 * Uses {@link PasswordHash} to take care of hashing passwords.
 */
public final class PostgresUserStorage implements UserStorage {

    private static final Logger log = LoggerFactory.getLogger( PostgresUserStorage.class );

    private final PostgresUsersDAO dao;
    private final PostgresLoginsStorage loginsStorage;

    public PostgresUserStorage( PostgresUsersDAO dao,
                                PostgresLoginsStorage loginsStorage ) {
        this.dao = dao;
        this.loginsStorage = loginsStorage;
    }

    @Nullable
    @Override
    public PersistedUser getByCredentials( @Nonnull UserCredentials userCredentials )
            throws StorageException {
        @Nullable Users users;

        try {
            users = dao.getByUsername( userCredentials.getUsername() );
        } catch ( DBIException | PSQLException e ) {
            throw storageExceptionFrom( e );
        }

        if ( !verifyPassword( userCredentials, users ) ) {
            log.debug( users == null ?
                    "User authentication failed: user does not exist." :
                    "Login failed: user presented wrong credentials." );

            return null;
        }

        log.debug( "User successfully authenticate: {}", userCredentials.getUsername() );
        loginsStorage.insertFor( usersToPersistedUser( users ) );

        return usersToPersistedUser( users );
    }

    private boolean verifyPassword( UserCredentials credentials, @Nullable Users users ) {
        // generate a random salt in case the user does not exist just to waste time verifying
        // an impossible match
        byte[] storedSalt = users == null ? PasswordHash.generateSalt() : users.getSalt();

        byte[] presentedPassword = PasswordHash.hash( credentials.getPassword(), storedSalt );

        return users != null && Arrays.equals( presentedPassword, users.getPassword() );
    }

    @Nonnull
    @Override
    public PersistedUser createUser( @Nonnull User user,
                                     @Nonnull String plainTextPassword )
            throws StorageException {
        byte[] salt = PasswordHash.generateSalt();
        byte[] password = PasswordHash.hash( plainTextPassword, salt );

        try {
            dao.create( user.getUsername(), user.getPhone(), password, salt );
            String id = dao.getIdOfUser( user.getUsername() );

            return new PersistedUser( user, id );
        } catch ( DBIException | PSQLException e ) {
            throw storageExceptionFrom( e );
        }
    }

    @Nullable
    @Override
    public PersistedUser getById( @Nonnull String userId )
            throws StorageException {
        @Nullable Users users;

        try {
            users = dao.getById( userId );
        } catch ( DBIException | PSQLException e ) {
            throw storageExceptionFrom( e );
        }

        return usersToPersistedUser( users );
    }

    @Nullable
    private PersistedUser usersToPersistedUser( @Nullable Users users ) {
        return users == null ? null : new PersistedUser(
                new User( users.getUsername(), users.getPhonenumber() ), users.getUserid() );
    }
}
