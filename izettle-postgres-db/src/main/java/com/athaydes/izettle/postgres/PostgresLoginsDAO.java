package com.athaydes.izettle.postgres;


import org.postgresql.util.PSQLException;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/**
 * Data Access Object for the logins table.
 */
@RegisterMapper( LoginsMapper.class )
public interface PostgresLoginsDAO {

    @SqlQuery( "select * from logins where \"username\" = :username " +
            "order by time ASC limit :count" )
    List<Logins> getByUserName( @Bind( "username" ) String username,
                                @Bind( "count" ) int limit ) throws PSQLException;

    @SqlUpdate( "insert into logins (\"username\", time) values(:username, now())" )
    void insert( @Bind( "username" ) String username ) throws PSQLException;

}
