package com.athaydes.izettle.postgres;

import com.athaydes.izettle.api.storage.LoginDataStorage;
import com.athaydes.izettle.api.storage.StorageException;
import com.athaydes.izettle.data.LoginData;
import com.athaydes.izettle.data.PersistedUser;
import org.postgresql.util.PSQLException;
import org.skife.jdbi.v2.exceptions.DBIException;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Collectors;

import static com.athaydes.izettle.postgres.error.PostgresErrorMapper.storageExceptionFrom;

/**
 * PostGres storage for login entries.
 */
public final class PostgresLoginsStorage implements LoginDataStorage {

    private final PostgresLoginsDAO loginsDAO;

    public PostgresLoginsStorage( PostgresLoginsDAO loginsDAO ) {
        this.loginsDAO = loginsDAO;
    }

    @Nonnull
    @Override
    public List<LoginData> getByUserName( @Nonnull String username,
                                          int limit ) throws StorageException {

        try {
            return loginsDAO.getByUserName( username, limit ).stream()
                    .map( entry -> new LoginData( username, entry.getTime().toInstant() ) )
                    .collect( Collectors.toList() );
        } catch ( DBIException | PSQLException e ) {
            throw storageExceptionFrom( e );
        }
    }

    @Override
    public void insertFor( @Nonnull PersistedUser user )
            throws StorageException {
        try {
            loginsDAO.insert( user.getUser().getUsername() );
        } catch ( DBIException | PSQLException e ) {
            throw storageExceptionFrom( e );
        }
    }
}
