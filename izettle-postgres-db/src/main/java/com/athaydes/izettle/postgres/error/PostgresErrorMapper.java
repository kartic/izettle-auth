package com.athaydes.izettle.postgres.error;

import com.athaydes.izettle.api.storage.StorageException;
import org.postgresql.util.PSQLException;
import org.skife.jdbi.v2.exceptions.DBIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A helper class to map PostGres error messages to values that are safe to show to users.
 */
public class PostgresErrorMapper {

    private static final Logger log = LoggerFactory.getLogger( PostgresErrorMapper.class );

    public static StorageException storageExceptionFrom( Exception exception ) {
        if ( exception instanceof DBIException ) {
            Throwable cause = exception.getCause();
            if ( cause instanceof PSQLException ) {
                log.debug( "Caught DBIException wrapping PSQLException" );

                // let the code below handle this
                exception = ( Exception ) cause;
            }
        }

        if ( exception instanceof PSQLException ) {
            log.debug( "Turning PSQLException into StorageException" );
            return new StorageException( mapPostgresErrorToUserMessage( ( PSQLException ) exception ) );
        }

        log.warn( "Unable to handle Exception graciously" );

        // no idea how to handle this, just re-throw... this should not happen.
        throw new RuntimeException( exception );
    }

    private static String mapPostgresErrorToUserMessage( PSQLException exception ) {
        String message = exception.getMessage();
        if ( message.startsWith(
                "ERROR: duplicate key value violates unique constraint \"users_username_key\"" ) ) {
            return "Sorry, but looks like this username already exists! Please try another one.";
        }

        log.warn( "Cannot re-map message: {}", exception.getMessage() );
        return "A problem occurred trying to perform a storage operation";
    }
}
