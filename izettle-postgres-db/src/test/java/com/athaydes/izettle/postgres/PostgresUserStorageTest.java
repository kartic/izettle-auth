package com.athaydes.izettle.postgres;

import com.athaydes.izettle.api.authentication.UserCredentials;
import com.athaydes.izettle.api.storage.StorageException;
import com.athaydes.izettle.data.PersistedUser;
import com.athaydes.izettle.data.User;
import org.junit.Test;
import org.postgresql.util.PSQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

public class PostgresUserStorageTest {

    /**
     * Mockito mock can't do this.
     */
    private static class PostgresUsersDAOMock implements PostgresUsersDAO {

        final List<String> newUsernames = new ArrayList<>();
        final List<String> newPhones = new ArrayList<>();
        final List<byte[]> newPasswords = new ArrayList<>();
        final List<byte[]> newSalts = new ArrayList<>();
        Function<String, String> mockForGetIdOfUser;
        Function<String, Users> mockForGetById;
        Function<String, Users> mockForGetByUsername;

        @Override
        public Users getByUsername( String username ) throws PSQLException {
            return mockForGetByUsername.apply( username );
        }

        @Override
        public Users getById( String userId ) throws PSQLException {
            return mockForGetById.apply( userId );
        }

        @Override
        public void create( String username, String phone, byte[] password, byte[] salt )
                throws PSQLException {
            newUsernames.add( username );
            newPhones.add( phone );
            newPasswords.add( password );
            newSalts.add( salt );
        }

        @Override
        public String getIdOfUser( String username )
                throws PSQLException {
            return mockForGetIdOfUser.apply( username );
        }
    }

    @Test
    public void createsUserWithHashedAndSaltedPassword() throws StorageException {
        String expectedUserId = "user-id";
        User user = new User( "name", "123" );
        String password = "super-pass";

        PostgresUsersDAOMock usersDAOMock = new PostgresUsersDAOMock();
        usersDAOMock.mockForGetIdOfUser = name -> {
            if ( name.equals( user.getUsername() ) ) {
                return expectedUserId;
            } else {
                return "";
            }
        };

        PostgresLoginsDAO loginsDAOMock = mock( PostgresLoginsDAO.class );

        PostgresUserStorage userStorage = new PostgresUserStorage(
                usersDAOMock,
                new PostgresLoginsStorage( loginsDAOMock ) );

        // WHEN a new user is created
        PersistedUser persistedUser = userStorage.createUser( user, password );

        // THEN a persisted user is returned with the expected generated ID
        assertThat( persistedUser.getUser(), equalTo( user ) );
        assertThat( persistedUser.getUserId(), equalTo( expectedUserId ) );

        // AND a salt is generated and mixed into the password, hashing the password safely
        assertThat( usersDAOMock.newSalts.size(), is( 1 ) );
        assertThat( usersDAOMock.newPasswords.size(), is( 1 ) );
        String salt = new String( usersDAOMock.newSalts.get( 0 ) );
        String pass = new String( usersDAOMock.newPasswords.get( 0 ) );

        // as recommended by OWASP
        assertThat( salt.length(), greaterThan( 32 ) );
        assertThat( pass.length(), greaterThan( 16 ) );

        System.out.println( "SALT " + salt );
        System.out.println( "PASS " + pass );

        verifyZeroInteractions( loginsDAOMock );
    }

    @Test
    public void canVerifyPasswordButNotSoQuickly() throws StorageException, PSQLException {
        byte[] salt = PasswordHash.generateSalt();
        String password = "super-pass";

        User user = new User( "name", "123" );

        Users users = new Users();
        users.setUserid( "persisted-user-id" );
        users.setSalt( salt );
        users.setPassword( PasswordHash.hash( password, salt ) );
        users.setUsername( user.getUsername() );
        users.setPhonenumber( user.getPhone() );

        PostgresUsersDAOMock usersDAOMock = new PostgresUsersDAOMock();
        usersDAOMock.mockForGetByUsername = username -> {
            if ( username.equals( user.getUsername() ) ) {
                return users;
            } else {
                return null;
            }
        };

        PostgresLoginsDAO loginsDAOMock = mock( PostgresLoginsDAO.class );

        PostgresUserStorage userStorage = new PostgresUserStorage(
                usersDAOMock,
                new PostgresLoginsStorage( loginsDAOMock ) );

        // WHEN correct user's credentials are checked
        TimedValue<PersistedUser> persistedUserValue = timed( () ->
                userStorage.getByCredentials(
                        new UserCredentials( user.getUsername(), password ) ) );

        // THEN the persisted user is returned
        assertThat( persistedUserValue.value, notNullValue() );

        assertThat( persistedUserValue.value.getUserId(), equalTo( users.getUserid() ) );
        assertThat( persistedUserValue.value.getUser(), equalTo( user ) );

        // AND the login is inserted into the Logins Storage
        verify( loginsDAOMock, times( 1 ) )
                .insert( persistedUserValue.value.getUser().getUsername() );

        // WHEN incorrect credentials are presented
        TimedValue<PersistedUser> wrongCredentialsUser = timed( () ->
                userStorage.getByCredentials(
                        new UserCredentials( user.getUsername(), password + "!" ) ) );

        // AND users that do not exist try to login
        TimedValue<PersistedUser> nonExistingUser = timed( () ->
                userStorage.getByCredentials(
                        new UserCredentials( "nobody", password ) ) );

        // THEN in both cases, no user is found
        assertThat( wrongCredentialsUser.value, nullValue() );
        assertThat( nonExistingUser.value, nullValue() );

        // AND in all cases, verifying each password should take longer than 0.10 seconds
        assertThat( persistedUserValue.timeInMs, greaterThan( 100L ) );
        assertThat( wrongCredentialsUser.timeInMs, greaterThan( 100L ) );
        assertThat( nonExistingUser.timeInMs, greaterThan( 100L ) );

        // AND the logins Storage was not activated anymore
        verifyNoMoreInteractions( loginsDAOMock );
    }

    private static <V> TimedValue<V> timed( StorageSupplier<V> run ) throws StorageException {
        long start = System.currentTimeMillis();
        V value = run.get();
        long timeInMs = System.currentTimeMillis() - start;
        return new TimedValue<>( timeInMs, value );
    }

    @FunctionalInterface
    private interface StorageSupplier<T> {
        T get() throws StorageException;
    }

    private static class TimedValue<V> {
        final V value;
        final long timeInMs;

        TimedValue( long timeInMs, V value ) {
            this.value = value;
            this.timeInMs = timeInMs;
        }
    }
}