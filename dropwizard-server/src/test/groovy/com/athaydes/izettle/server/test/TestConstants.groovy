package com.athaydes.izettle.server.test

import com.athaydes.izettle.server.ClientDetails

/**
 * Useful test constants.
 */
interface TestConstants {
    String clientId = ClientDetails.CLIENT_ID
    String clientSecret = ClientDetails.CLIENT_SECRET
    String username = 'renato'
    String password = 'my-pass'
    String redirectUri = 'http://localhost:8082/oauth-callback'
}
