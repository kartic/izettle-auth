package com.athaydes.izettle.server.test

import com.athaydes.izettle.api.ServerSettings
import com.athaydes.izettle.test.client.CanAuthenticate
import com.athaydes.izettle.test.client.KnowsOAuthFlows
import com.google.common.net.HttpHeaders
import groovy.json.JsonSlurper
import okhttp3.OkHttpClient
import okhttp3.Request
import org.eclipse.jetty.http.HttpStatus
import spock.lang.Specification
import spock.lang.Unroll

import javax.ws.rs.core.MediaType
import javax.ws.rs.core.UriBuilder
import java.util.concurrent.atomic.AtomicReference

class LoginsEndpointIntegrationTest extends Specification
        implements IntegrationTest, CanAuthenticate, KnowsOAuthFlows {

    private final URI loginsEndpoint = URI.create( 'http://localhost:8080/' +
            ServerSettings.Endpoints.LOGINS )

    private final client = new OkHttpClient()

    @Override
    String getClientId() { TestConstants.clientId }

    @Override
    String getClientSecret() { TestConstants.clientSecret }

    // avoid requesting a token on every test
    static final AtomicReference<String> reusableTokenRef = new AtomicReference<>()

    def "User can see his own login data"() {
        when: 'The user uses the token to get his login data'
        def uriBuilder = UriBuilder.fromUri( loginsEndpoint )

        def request = new Request.Builder()
                .url( uriBuilder.build().toURL() )
                .header( HttpHeaders.AUTHORIZATION, "Bearer $reusableToken" )
                .header( HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON )
                .build()

        def loginsResponse = client.newCall( request ).execute()

        then: 'The logins data should be provided as JSON'
        loginsResponse.code() == 200
        def jsonBody = loginsResponse.body()

        jsonBody.contentType().toString() == MediaType.APPLICATION_JSON

        and: 'The JSON contents should show the logins data'
        def loginsData = new JsonSlurper().parseText( jsonBody.string() ) as Map
        loginsData.logins instanceof List
        List logins = loginsData.logins
        logins.size() == 2 // memory-based storage used for tests always has 2 entries
    }

    @Unroll
    def "Unauthenticated users cannot access any data"() {
        when: 'The user tries to get some login data with a invalid token or no token at all'
        def uriBuilder = UriBuilder.fromUri( loginsEndpoint )

        def requestBuilder = new Request.Builder()
                .url( uriBuilder.build().toURL() )
                .header( HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON )

        if ( token ) {
            requestBuilder.header( HttpHeaders.AUTHORIZATION, "Bearer $token" )
        }

        def request = requestBuilder.build()
        def loginsResponse = client.newCall( request ).execute()

        then: 'The server should report an unauthorized error'
        loginsResponse.code() == HttpStatus.UNAUTHORIZED_401

        and: 'The json body should contain an error message'
        def jsonBody = loginsResponse.body()

        jsonBody.contentType().toString() == MediaType.APPLICATION_JSON
        def json = new JsonSlurper().parseText( jsonBody.string() )
        json == [ error: 'Unauthorized' ]

        where:
        token << [ '', 'fake-token', '2#@23540=3-2=4k?34l1k3j534534kl;31lkkldfjakdl2@%^fjg;lt34' ]
    }

    def "User can see her own login data with a limit on the number of returned values"() {
        when: 'The user uses the token to get his login data'
        def uriBuilder = UriBuilder.fromUri( loginsEndpoint )

        def request = new Request.Builder()
                .url( uriBuilder.queryParam( 'count', 1 ).build().toURL() )
                .header( HttpHeaders.AUTHORIZATION, "Bearer $reusableToken" )
                .header( HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON )
                .build()

        def loginsResponse = client.newCall( request ).execute()

        then: 'The logins data should be provided as JSON'
        loginsResponse.code() == 200
        def jsonBody = loginsResponse.body()

        jsonBody.contentType().toString() == MediaType.APPLICATION_JSON

        and: 'The JSON contents should show the logins data, but only one as requested'
        def loginsData = new JsonSlurper().parseText( jsonBody.string() ) as Map
        loginsData.logins instanceof List
        List logins = loginsData.logins
        logins.size() == 1
    }

    private static final INVALID_COUNT = 'query param count must be between 1 and 1000'
    private static final NOT_FOUND = 'HTTP 404 Not Found'

    @Unroll
    def "A bad request occurs when an invalid count parameter is provided"() {
        when: 'The user uses the token to get his login data with an invalid count parameter'
        def uriBuilder = UriBuilder.fromUri( loginsEndpoint )

        def request = new Request.Builder()
                .url( uriBuilder.queryParam( 'count', invalidCount ).build().toURL() )
                .header( HttpHeaders.AUTHORIZATION, "Bearer $reusableToken" )
                .header( HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON )
                .build()

        def loginsResponse = client.newCall( request ).execute()

        then: 'The server should report a bad request error'
        loginsResponse.code() == expectedCode
        def jsonBody = loginsResponse.body()

        jsonBody.contentType().toString() == MediaType.APPLICATION_JSON

        and: 'The JSON contents should explain why'
        def json = new JsonSlurper().parseText( jsonBody.string() )
        json == [ errors: [ expectedMessage ] ]

        where:
        invalidCount | expectedMessage | expectedCode
        '-1'         | INVALID_COUNT   | HttpStatus.BAD_REQUEST_400
        '-23'        | INVALID_COUNT   | HttpStatus.BAD_REQUEST_400
        '0'          | INVALID_COUNT   | HttpStatus.BAD_REQUEST_400
        '1000000'    | INVALID_COUNT   | HttpStatus.BAD_REQUEST_400
        'Joe'        | NOT_FOUND       | HttpStatus.NOT_FOUND_404
        '0x100101'   | NOT_FOUND       | HttpStatus.NOT_FOUND_404
        'EFF'        | NOT_FOUND       | HttpStatus.NOT_FOUND_404
        '123A'       | NOT_FOUND       | HttpStatus.NOT_FOUND_404
    }

    private String obtainToken() {
        def authResponse = authenticate(
                TestConstants.username, TestConstants.password, clientId )
        def code = extractCodeFrom( authResponse )
        def tokenResponse = exchangeCodeForToken( code )

        def json = new JsonSlurper().parseText( tokenResponse.body().string() ) as Map
        assert json.access_token instanceof String
        return json.access_token
    }

    String getReusableToken() {
        if ( reusableTokenRef.get() == null ) {
            println "Obtaining an access token"
            reusableTokenRef.set obtainToken()
        }
        reusableTokenRef.get()
    }

}
