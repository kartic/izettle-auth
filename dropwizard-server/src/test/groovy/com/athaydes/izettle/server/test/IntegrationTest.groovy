package com.athaydes.izettle.server.test

import com.athaydes.izettle.server.AuthorizationServer
import com.athaydes.izettle.test.client.VerifiesServerHealth
import groovy.transform.CompileStatic
import groovy.transform.SelfType
import spock.lang.Specification

import java.time.Duration

/**
 * Trait used by integration tests that require a live server.
 */
@SelfType( Specification )
@CompileStatic
trait IntegrationTest implements VerifiesServerHealth {

    static AuthorizationServer server

    abstract String getClientId()

    abstract String getClientSecret()

    def setupSpec() {
        def projectDir = System.getProperty( 'project.rootDir' )

        if ( !projectDir ) {
            throw new RuntimeException( 'Integration tests cannot run without the "project.rootDir" being set!' )
        }

        def configFile = new File( projectDir, 'test-server-config.yaml' )

        server = new AuthorizationServer()
        server.run( 'server', configFile.absolutePath )

        waitForServerToBeHealthy( Duration.ofSeconds( 5 ) )
    }

    def cleanupSpec() {
        server.stop()
    }

}