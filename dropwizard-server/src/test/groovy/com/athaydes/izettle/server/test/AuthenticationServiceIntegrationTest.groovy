package com.athaydes.izettle.server.test

import com.athaydes.izettle.test.client.CanAuthenticate
import org.eclipse.jetty.http.HttpStatus
import spock.lang.Specification
import spock.lang.Unroll

import javax.ws.rs.core.MediaType

class AuthenticationServiceIntegrationTest extends Specification
        implements CanAuthenticate, IntegrationTest {

    @Override
    String getClientId() { TestConstants.clientId }

    @Override
    String getClientSecret() { TestConstants.clientSecret }

    def "User should be able to authenticate using valid credentials"() {
        when: 'A user authenticates with the default, valid credentials'
        final providedState = useState ? 'our-state' : null
        def response = authenticate( TestConstants.username, TestConstants.password,
                clientId, null, providedState )

        then: 'The user gets redirected to the server configured redirectUri with a code nonce'
        response.code() == 303

        def location = response.header( 'Location' )
        location != null

        def locationUri = URI.create( location )
        locationUri.authority == 'localhost:8082'
        locationUri.scheme == 'http'
        locationUri.path == '/oauth-callback'

        def queryParts = locationUri.query.split( '&' ).toList()
        def codePart = queryParts.find { it.startsWith( 'code' ) }
        def statePart = queryParts.find { it.startsWith( 'state' ) }
        def codeParts = codePart?.split( '=' )
        def stateParts = statePart?.split( '=' )

        codeParts?.size() == 2 && codeParts[ 0 ] == 'code'

        if ( useState ) {
            assert stateParts?.size() == 2 && stateParts[ 0 ] == 'state'
        } else {
            assert stateParts == null
        }

        and: 'The nonce should be longer than 10 characters'
        codeParts[ 1 ].size() > 10

        and: 'If the state was provided, the same state should be returned'
        if ( useState ) {
            assert stateParts[ 1 ] == providedState
        }

        where: 'We attempt logging in both using and not using state'
        useState << [ false, true ]
    }

    def "User should not be able to authenticate with wrong credentials"() {
        when: 'A user authenticates with invalid credentials'
        def response = authenticate( 'wrong-user', 'bad-pass', clientId )

        then: 'The user gets back to the login page'
        response.code() == 200
        def body = response.body()
        body.contentType().toString().startsWith( 'text/html' )

        def bodyContents = body.string()
        bodyContents.contains( '<title>Log in</title>' )

        and: 'The login page contains an error "Bad credentials"'
        bodyContents.contains( '<div class="error">Bad credentials</div>' )
    }

    def "User should not be able to authenticate without giving credentials"() {
        when: 'A user authenticates with invalid credentials'
        def response = authenticate( '', '', clientId )

        then: 'The user gets back to the login page'
        response.code() == 200
        def body = response.body()
        body.contentType().toString().startsWith( 'text/html' )

        def bodyContents = body.string()
        bodyContents.contains( '<title>Log in</title>' )

        and: 'The login page contains an error "Bad credentials"'
        bodyContents.contains( '<div class="error">Please enter your username and password</div>' )
    }

    def "The 400 - Bad Request Page should be shown if the user-agent does not provide a clientId"() {
        when: 'A user tries to submit a form without a clientId'
        def clientId = ''
        def response = authenticate( 'joe', 'doe', clientId )

        then: 'The user sees the 400 - Bad Request Page'
        response.code() == 400
        def body = response.body()
        body.contentType().toString().startsWith( 'text/html' )

        def bodyContents = body.string()
        bodyContents.contains( '<title>Error</title>' )
        bodyContents.contains( '<h2>400 - Bad Request</h2>' )
        bodyContents.contains( 'form field clientId may not be empty' )
    }

    @Unroll
    def "The 401 - Unauthorized Page (HTML) should be shown if the user-agent provides a bad clientId"() {
        when: 'A user tries to submit a form with a bad clientId'
        def response = authenticate( 'joe', 'doe', clientId )

        then: 'The user sees the 401 - Unauthorized Page'
        response.code() == HttpStatus.UNAUTHORIZED_401
        def body = response.body()
        body.contentType().toString().startsWith( MediaType.TEXT_HTML )

        def bodyContents = body.string()
        bodyContents.contains( 'Client is not authorized' )

        where: 'the clientId is incorrect'
        clientId << [ 'hello', 'XSWERVZXCV', '!@!@^$%^$%$#$%^' ]
    }

    @Unroll
    def "The 401 - JSON Response should be shown if the user-agent provides a bad clientId"() {
        when: 'A user tries to submit a form with a bad clientId'
        def response = authenticate( 'joe', 'doe', clientId )

        then: 'The user sees the 401 - Unauthorized Page'
        response.code() == HttpStatus.UNAUTHORIZED_401
        def body = response.body()
        body.contentType().toString().startsWith( MediaType.TEXT_HTML )

        def bodyContents = body.string()
        bodyContents.contains( 'Client is not authorized' )

        where: 'the clientId is incorrect'
        clientId << [ 'hello', 'XSWERVZXCV', '!@!@^$%^$%$#$%^' ]
    }

}
