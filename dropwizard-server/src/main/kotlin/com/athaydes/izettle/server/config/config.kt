package com.athaydes.izettle.server.config

import com.fasterxml.jackson.annotation.JsonProperty
import io.dropwizard.Configuration
import io.dropwizard.db.DataSourceFactory
import org.hibernate.validator.constraints.NotEmpty
import org.hibernate.validator.constraints.URL
import java.net.URI
import java.util.*
import javax.validation.Valid
import javax.validation.constraints.Min


/**
 * OAuth-specific configuration.
 */
class OAuthConfiguration {

    /**
     * The OAuth redirect URI (for simplicity, only one URI is allowed,
     * which the OAuth clients must use for the single application supported by this server).
     */
    @JsonProperty
    @URL
    @NotEmpty
    var redirectUri: URI = URI.create("http://localhost:8082/oauth-callback")

    /**
     * Access token TTL in seconds.
     */
    @Min(value = 1)
    @JsonProperty
    var accessTokenTimeToLive: Long = 3600L

}

/**
 * Server top-level configuration.
 */
class ServerConfiguration : Configuration() {

    /**
     * Nonce TTL in seconds.
     *
     * Nonces are one-time-use tokens, normally used for authentication.
     *
     * @see com.athaydes.izettle.api.storage.NonceStorage
     */
    @Min(value = 1)
    @JsonProperty
    var nonceTimeToLive: Long = 30L

    /**
     * Whether to run the server with the [com.athaydes.izettle.server.memory_storage.MemoryBasedUserStorage]
     * rather than a real database storage.
     *
     * Defaults to false! Set to true for running tests.
     */
    @JsonProperty
    var useMemoryBasedUserStorage = false

    /**
     * The OAuth-specific configuration.
     */
    @JsonProperty
    var oauthConfiguration = OAuthConfiguration()

    /**
     * The database configuration.
     * Mandatory only if not using memory-based storage.
     */
    @Valid
    @JsonProperty("database")
    var database: Optional<DataSourceFactory> = Optional.empty()

}