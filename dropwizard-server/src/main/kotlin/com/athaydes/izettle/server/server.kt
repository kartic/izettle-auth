package com.athaydes.izettle.server

import com.athaydes.izettle.api.authentication.AuthenticationService
import com.athaydes.izettle.api.authentication.ClientConfig
import com.athaydes.izettle.api.endpoints.LoginsEndpoint
import com.athaydes.izettle.api.endpoints.RegisterEndpoint
import com.athaydes.izettle.api.error.ValidationErrorMessageBodyWriter
import com.athaydes.izettle.api.error.WebApplicationExceptionMapper
import com.athaydes.izettle.api.oauth.AuthorizationEndpoint
import com.athaydes.izettle.api.oauth.TokenEndpoint
import com.athaydes.izettle.api.storage.LoginDataStorage
import com.athaydes.izettle.api.storage.NonceStorage
import com.athaydes.izettle.api.storage.UserStorage
import com.athaydes.izettle.api.token.TokenService
import com.athaydes.izettle.data.LoggedInUserName
import com.athaydes.izettle.postgres.PostgresLoginsDAO
import com.athaydes.izettle.postgres.PostgresLoginsStorage
import com.athaydes.izettle.postgres.PostgresUserStorage
import com.athaydes.izettle.postgres.PostgresUsersDAO
import com.athaydes.izettle.server.config.ServerConfiguration
import com.athaydes.izettle.server.memory_storage.MemoryBasedLoginsStorage
import com.athaydes.izettle.server.memory_storage.MemoryBasedNonceStorage
import com.athaydes.izettle.server.memory_storage.MemoryBasedTokenService
import com.athaydes.izettle.server.memory_storage.MemoryBasedUserStorage
import com.athaydes.izettle.server.oauth.NoOpAuthorizer
import com.athaydes.izettle.server.oauth.OAuthAuthenticator
import com.athaydes.izettle.server.oauth.SimpleUnauthorizedHandler
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.dropwizard.Application
import io.dropwizard.auth.AuthDynamicFeature
import io.dropwizard.auth.AuthValueFactoryProvider
import io.dropwizard.auth.oauth.OAuthCredentialAuthFilter
import io.dropwizard.jdbi.DBIFactory
import io.dropwizard.migrations.MigrationsBundle
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import io.dropwizard.views.ViewBundle
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature
import org.slf4j.LoggerFactory
import java.time.Duration


/**
 * Hardcode some settings here for now... this is secret information, find a way to read this from the
 * environment rather than the code (not config!).
 */
object ClientDetails {
    const val CLIENT_ID = "8a0be41f7b70bdd49512"
    const val CLIENT_SECRET = "2a507f5da8c71ce91f3b2241744b9a7be501dee6"
}

class AuthorizationServer : Application<ServerConfiguration>() {

    private val log = LoggerFactory.getLogger(AuthorizationServer::class.java)

    private var env: Environment? = null

    override fun initialize(bootstrap: Bootstrap<ServerConfiguration>) {
        log.info("Initializing")

        bootstrap.objectMapper.registerKotlinModule()
        bootstrap.addBundle(ViewBundle<ServerConfiguration>())

        log.info("Installing migrations bundle")

        bootstrap.addBundle(object : MigrationsBundle<ServerConfiguration>() {
            override fun getDataSourceFactory(configuration: ServerConfiguration) =
                    if (configuration.useMemoryBasedUserStorage || !configuration.database.isPresent) null
                    else configuration.database.get()
        })
    }

    override fun run(configuration: ServerConfiguration,
                     environment: Environment) {
        log.info("Running Server")

        env = environment

        val secretConfig = ClientConfig(
                ClientDetails.CLIENT_ID,
                ClientDetails.CLIENT_SECRET,
                configuration.oauthConfiguration.redirectUri)

        val tokenService: TokenService = MemoryBasedTokenService(
                Duration.ofSeconds(configuration.oauthConfiguration.accessTokenTimeToLive))

        val nonceStorage: NonceStorage = MemoryBasedNonceStorage(
                Duration.ofSeconds(configuration.nonceTimeToLive))

        val (userStorage, loginsStore) = createDataStores(configuration, environment)

        val authenticationService = AuthenticationService(
                nonceStorage, userStorage, secretConfig)

        val authorizationEndpoint = AuthorizationEndpoint(secretConfig)

        val tokenEndpoint = TokenEndpoint(nonceStorage, tokenService, secretConfig)

        val loginsEndpoint = LoginsEndpoint(loginsStore)
        val registrationEndpoint = RegisterEndpoint(userStorage, secretConfig)

        log.info("Registering all endpoints")
        environment.jersey().register(authenticationService)
        environment.jersey().register(authorizationEndpoint)
        environment.jersey().register(tokenEndpoint)
        environment.jersey().register(loginsEndpoint)
        environment.jersey().register(registrationEndpoint)

        log.info("Registering custom error handlers")
        environment.jersey().register(ValidationErrorMessageBodyWriter)
        environment.jersey().register(WebApplicationExceptionMapper::class.java)

        log.info("Registering OAuth module")
        environment.jersey().register(AuthDynamicFeature(
                OAuthCredentialAuthFilter.Builder<LoggedInUserName>()
                        .setAuthenticator(OAuthAuthenticator(tokenService))
                        .setAuthorizer(NoOpAuthorizer())
                        .setPrefix("Bearer")
                        .setUnauthorizedHandler(SimpleUnauthorizedHandler)
                        .buildAuthFilter()))

        environment.jersey().register(
                RolesAllowedDynamicFeature::class.java)

        environment.jersey().register(
                AuthValueFactoryProvider.Binder(LoggedInUserName::class.java))

        log.info("Registering health checks")
        environment.healthChecks().register("basic-config", BasicConfigHealthCheck(configuration))

        log.info("Server setup completed")

        if (configuration.useMemoryBasedUserStorage) {
            log.warn("Using memory-based user storage... this is not safe for production")
        }
    }

    fun createDataStores(configuration: ServerConfiguration,
                         environment: Environment): Pair<UserStorage, LoginDataStorage> {
        return if (configuration.useMemoryBasedUserStorage) {
            MemoryBasedUserStorage() to MemoryBasedLoginsStorage()
        } else {
            ensureDatabaseConfiguration(configuration.database.isPresent)
            val factory = DBIFactory()
            val userStorage = createPostgresUserStorage(environment, configuration, factory)
            val loginsStore = createPostgresLoginsStorage(environment, configuration, factory)

            userStorage to loginsStore
        }
    }

    private fun createPostgresUserStorage(environment: Environment,
                                          configuration: ServerConfiguration,
                                          factory: DBIFactory): UserStorage {

        val jdbi = factory.build(environment, configuration.database.get(), "user-storage")

        val postgresUsersDAO = jdbi.onDemand(PostgresUsersDAO::class.java)
        val postgresLoginDAO = jdbi.onDemand(PostgresLoginsDAO::class.java)

        return PostgresUserStorage(postgresUsersDAO,
                PostgresLoginsStorage(postgresLoginDAO))
    }

    private fun createPostgresLoginsStorage(environment: Environment,
                                            configuration: ServerConfiguration,
                                            factory: DBIFactory): PostgresLoginsStorage {
        ensureDatabaseConfiguration(configuration.database.isPresent)

        val jdbi = factory.build(environment, configuration.database.get(), "logins-storage")

        val postgresLoginDAO = jdbi.onDemand(PostgresLoginsDAO::class.java)

        return PostgresLoginsStorage(postgresLoginDAO)
    }

    private fun ensureDatabaseConfiguration(dataBaseConfigPresent: Boolean) {
        if (!dataBaseConfigPresent) {
            log.error("Cannot create Postgres data source, no database has been configured")
            throw IllegalStateException("No database configured, cannot create Postgres data source")
        }
    }

    fun stop() {
        env?.applicationContext?.server?.stop()
    }
}

fun main(args: Array<String>) {
    AuthorizationServer().run(*args)
}