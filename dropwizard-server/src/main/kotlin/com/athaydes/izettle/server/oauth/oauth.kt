package com.athaydes.izettle.server.oauth

import com.athaydes.izettle.api.token.TokenService
import com.athaydes.izettle.data.LoggedInUserName
import io.dropwizard.auth.Authenticator
import io.dropwizard.auth.Authorizer
import io.dropwizard.auth.UnauthorizedHandler
import java.time.Instant
import java.util.Optional
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.UNAUTHORIZED

/**
 * No-op authorizer (everyone is authorized).
 */
class NoOpAuthorizer : Authorizer<LoggedInUserName> {

    override fun authorize(principal: LoggedInUserName,
                           role: String): Boolean {
        return true // TODO no role verification yet
    }
}

/**
 * Basic implementation of OAuth authenticator.
 */
class OAuthAuthenticator(
        val tokenService: TokenService) :
        Authenticator<String, LoggedInUserName> {
    override fun authenticate(credentials: String): Optional<LoggedInUserName> {

        val token = tokenService.verify(credentials)
        if (token != null && token.expiresAt.isAfter(Instant.now())) {
            val username = token.sub
            return Optional.of(LoggedInUserName(username))
        }

        return Optional.empty()
    }

}

object SimpleUnauthorizedHandler : UnauthorizedHandler {

    override fun buildResponse(prefix: String,
                               realm: String) =
            Response.status(UNAUTHORIZED)
                    .entity(mapOf("error" to "Unauthorized"))
                    .build()
}
