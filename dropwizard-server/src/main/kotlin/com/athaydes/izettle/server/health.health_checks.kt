package com.athaydes.izettle.server

import com.athaydes.izettle.server.config.ServerConfiguration
import com.codahale.metrics.health.HealthCheck

class BasicConfigHealthCheck(val configuration: ServerConfiguration) : HealthCheck() {

    override fun check(): Result {
        // either one or the other must be true, otherwise the configuration is invalid
        val ok = configuration.useMemoryBasedUserStorage || configuration.database.isPresent

        return if (ok) Result.healthy() else Result.unhealthy(
                "Inconsistent configuration: " +
                        "useMemoryBasedUserStorage set to ${configuration.useMemoryBasedUserStorage} AND " +
                        "database is${if (configuration.database.isPresent) "" else " NOT"} present.")
    }
}