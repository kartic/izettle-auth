package com.athaydes.izettle.browser_tests.traits

import com.athaydes.izettle.browser_tests.LoginPage
import com.athaydes.izettle.browser_tests.LogoutPage
import com.athaydes.izettle.browser_tests.RegistrationPage
import geb.spock.GebSpec
import groovy.transform.Immutable
import groovy.transform.SelfType

@Immutable
class AppUser {
    String username
    String password
    String phoneNumber
}

/**
 * A trait that can be used by tests to register and automatically create new users
 * that can use the website logged in.
 */
// this means this trait can only be applied to a GebSpec subclass
@SelfType( GebSpec )
trait CanUseWebsite {

    AppUser createAppUser( String username = UUID.randomUUID().toString(),
                           String password = UUID.randomUUID().toString(),
                           String phone = '555555555' ) {
        AppUser user = new AppUser( username, password, phone )

        browser.with {
            // go to a different page first to avoid form re-submission
            go LoginPage.url

            go RegistrationPage.url
            def page = at RegistrationPage

            page.usernameField.value( user.username )
            page.phoneField.value( user.phoneNumber )
            page.passwordField.value( user.password )
            page.sendButton.click()
        }

        return user
    }

    AppUser createAppUserAndLogin( String username = UUID.randomUUID().toString(),
                                   String password = UUID.randomUUID().toString(),
                                   String phone = '555555555' ) {
        def user = createAppUser( username, password, phone )
        login( user )
        return user
    }

    def login( AppUser user ) {
        logout()

        browser.with {
            def page = at LogoutPage
            page.authenticateLink.click()
            page = at LoginPage
            page.usernameField.value( user.username )
            page.passwordField.value( user.password )
            page.sendButton.click()
        }
    }

    def logout() {
        browser.with {
            go LogoutPage.url
        }
    }

}
