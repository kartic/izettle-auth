package com.athaydes.izettle.browser_tests

import geb.Page

class LoginPage extends Page {

    static url = "http://localhost:8080/authenticate"

    static at = { title == "Log in" }

    static content = {
        usernameField { $( "input[name=username]" ) }
        passwordField { $( "input[name=password]" ) }
        sendButton { $( "button[type=submit]" ) }
        infoMessages { $( 'div.info' ) }
        errorMessages { $( 'div.error' ) }
    }

}

class RegistrationPage extends Page {

    static url = "http://localhost:8080/registration"

    static at = { title == "Register" }

    static content = {
        usernameField { $( "input[name=username]" ) }
        passwordField { $( "input[name=password]" ) }
        phoneField { $( "input[name=phone]" ) }
        errorMessages(required: false) { $( "div.error" ) }
        sendButton { $( "button[type=submit]" ) }
    }

}

class FrontPage extends Page {

    static url = "http://localhost:8082/index"

    static at = { title == "Simple App" }

    static content = {
        greeting { $( 'div#greeting' ) }
        pageContent { $( 'div#content' ) }
    }

}

class LogoutPage extends Page {
    static url = "http://localhost:8082/logout"

    static at = { title == "Simple App" }

    static content = {
        pageContent { $( 'div#content' ) }
        authenticateLink { $( 'a#login-link' ) }
    }
}

class LoginsDataPage extends Page {
    static url = "http://localhost:8082/logins"

    static at = { title == "Simple App" }

    static content = {
        greeting { $( 'div#greeting' ) }
        pageContent { $( 'div#content' ) }
        loginData { $( 'div#content > ul > li' ) }
    }
}