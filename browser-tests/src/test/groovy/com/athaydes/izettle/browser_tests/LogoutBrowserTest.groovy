package com.athaydes.izettle.browser_tests

import com.athaydes.izettle.browser_tests.traits.CanUseWebsite
import com.athaydes.izettle.browser_tests.traits.RequiresRunningServer
import geb.spock.GebSpec

class LogoutBrowserTest extends GebSpec
        implements CanUseWebsite, RequiresRunningServer {

    def "A logged in user should be able to log out"() {
        given: 'A logged in user'
        createAppUserAndLogin()

        when: 'The user logs out'
        logout()

        then: 'The user should see a message confirming logout'
        def page = at LogoutPage
        page.pageContent.text() == 'You are logged out!'
    }

}
