package com.athaydes.izettle.browser_tests.traits

import com.athaydes.izettle.server.AuthorizationServer
import com.athaydes.izettle.test.client.VerifiesServerHealth
import geb.spock.GebSpec
import groovy.transform.SelfType

import java.time.Duration

/**
 * This trait allows any test to require a running server instance to be started
 * before it runs, and stopped after.
 */
@SelfType( GebSpec )
trait RequiresRunningServer extends VerifiesServerHealth {

    static AuthorizationServer server

    def setupSpec() {
        def projectDir = System.getProperty( 'project.rootDir' )

        if ( !projectDir ) {
            throw new RuntimeException( 'Browser tests cannot run without the "project.rootDir being set!' )
        }

        def configFile = new File( projectDir, 'browser-test-server-config.yaml' )

        server = new AuthorizationServer()
        server.run( 'server', configFile.absolutePath )

        waitForServerToBeHealthy( Duration.ofSeconds( 10 ) )
    }

    def setup() {
        baseUrl = 'http://localhost:8080'
    }

    def cleanupSpec() {
        server?.stop()
    }

}
