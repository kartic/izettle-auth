package com.athaydes.izettle.browser_tests

import com.athaydes.izettle.browser_tests.traits.CanUseWebsite
import com.athaydes.izettle.browser_tests.traits.RequiresRunningServer
import geb.spock.GebSpec

/**
 * Test for error conditions on registration.
 *
 * Successful registration is checked in every test that needs to create a user
 * and log in, which is most tests.
 */
class BrowserRegistrationSpec extends GebSpec
        implements RequiresRunningServer, CanUseWebsite {

    def "User cannot register with existing username"() {
        when: 'User Fred register his username'
        createAppUser( 'Fred' )

        then: 'Fred is accepted and sees the login page'
        at LoginPage

        when: 'Another Fred comes along and tries to register as well'
        createAppUser( 'Fred' )

        then: 'The second Fred should remain in the registration page and see an error explaining why'
        def page = at RegistrationPage
        page.errorMessages.size() == 1
        page.errorMessages.first().text().contains( 'username already exists' )
    }

}
