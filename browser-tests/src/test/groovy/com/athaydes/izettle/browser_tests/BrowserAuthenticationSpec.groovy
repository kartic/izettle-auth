package com.athaydes.izettle.browser_tests

import com.athaydes.izettle.browser_tests.traits.RequiresRunningServer
import geb.spock.GebSpec

class BrowserAuthenticationSpec extends GebSpec
        implements RequiresRunningServer {

    def "User should be able to authenticate and see the front page"() {
        given: 'A user goes to the registration page'
        go RegistrationPage.url
        def page = at RegistrationPage

        and: 'the user enters her details'
        page.usernameField.value( 'mary' )
        page.phoneField.value( '444' )
        page.passwordField.value( '123' )

        when: 'The user submits the form'
        page.sendButton.click()
        page = at LoginPage

        then: 'The user should be sent to the login page with a welcome message'
        page.infoMessages.size() == 1
        page.infoMessages.text().contains( 'Congratulations' )

        when: 'The user logs in with correct credentials'
        page.usernameField.value( 'mary' )
        page.passwordField.value( '123' )
        page.sendButton.click()

        then: 'The user is redirected to the initial page of the app'
        def frontPage = at FrontPage

        and: 'The user should see a greeting confirming login'
        frontPage.greeting.text() == "Hello, ${'mary'}!"
    }

    def "User cannot authenticate with wrong credentials"() {
        given: 'A user goes to the registration page'
        go RegistrationPage.url
        def page = at RegistrationPage

        and: 'the user enters her details'
        page.usernameField.value( 'jenny' )
        page.passwordField.value( 'jen1982' )

        when: 'The user submits the form'
        page.sendButton.click()
        page = at LoginPage

        and: 'The user tries to log in with bad credentials'
        page.usernameField.value( 'jenny' )
        page.passwordField.value( 'jenny1982' )
        page.sendButton.click()

        then: 'The user is sees the login page again with an error'
        at LoginPage
        page.errorMessages.size() == 1
        page.errorMessages.first().text() == 'Bad credentials'
    }

}
